//? Mantendrá la configuracion del servidor.
'use strict'

//llamar las librerias de body-parser

const bodyParser = require('body-parser');
const express = require('express');
const app = express(); 

//activar los cores

app.use(function (req, res, next) {
res.header("Access-Control-Allow-Origin","*");

res.header("Access-Control-Allow-Header","Origin, X-Requested-Width,Content-Type, Accept");

res.header("Access-Control-Allow-Methods","GET,POST,OPTIONS,PUT,PATCH,DELETE");
next();


});

//activar los middlewares de express

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json());
app.use(require("../Routes/routes"));
module.exports =app;




module.exports = app;