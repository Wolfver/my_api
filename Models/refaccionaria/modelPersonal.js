const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let PersonalRefaccionaria = new Schema({
    IDPersonal: {type: Number},
    nombrePersonal: {type: String},
    apellidoPersonal: {type: String},
    puestoPersonal: {type: String},
    telefonoPersonal: {type: String},
});


module.exports = mongoose.model('nuevoPersonal', PersonalRefaccionaria)