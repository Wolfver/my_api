const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let ProveedorRefaccionaria = new Schema({
    IDProveedor: {type: Number},
    nombreProveedor: {type: String},
    marcaProveedor: {type: String},
    direccionProveedor: {type: String},
    telefonoProveedor: {type: String},
});


module.exports = mongoose.model('nuevoProveedor', ProveedorRefaccionaria)