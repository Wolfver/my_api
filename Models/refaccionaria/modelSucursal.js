const mongoose = require('mongoose');

let Schema = mongoose.Schema;

let SucursalRefaccionaria = new Schema({
    IDSucursal: {type: Number},
    nombreSucursal: {type: String},
    estadoSucursal: {type: String},
    direccionSucursal: {type: String},
    telefonoSucursal: {type: String},
});


module.exports = mongoose.model('nuevoSucursal', SucursalRefaccionaria)