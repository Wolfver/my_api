//? Config, guardará las clases y constantes de Clusters en la nube, entre otras cosas.
'use strict'

const mongoose = require('mongoose');
const app = require('../Server/index');
const port = 3900;

//Generar una promesa global
mongoose.Promise = global.Promise;

//Hacer la conexion a la DB
mongoose.connect('mongodb://127.0.0.1:27017/refaccionaria', {useNewUrlParser: true})
    .then(() => {
        console.log('Base de datos corriendo');

        //Escuchar el puerto del server
        app.listen(port, () => {
            console.log(`Server corriendo en el puerto: ${port}`);
        });
    });