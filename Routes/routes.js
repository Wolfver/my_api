//? Routes va a contener los metodos GET, POST, PUT, DELETE, UPDATE
const express = require('express');
const app = express();


app.use(require('./test'));
app.use(require('./productoNuevo/routeNuevoProducto'));
app.use(require('./productoNuevo/routePersonal'));
app.use(require('./productoNuevo/routeSucursal'));
app.use(require('./productoNuevo/routeProveedor'));


module.exports = app;