const express = require('express');
//const modelPersonal = require('../../Models/refaccionaria/modelPersonal');
const { request } = require('../test');
const modelPersonal = require('../../Models/refaccionaria/modelPersonal');

let app = express();

app.post('/personal/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaPersonal = new modelPersonal({
        IDPersonal: body.IDPersonal,
        nombrePersonal: body.nombrePersonal,
        apellidoPersonal: body.apellidoPersonal,
        puestoPersonal: body.puestoPersonal,
        telefonoPersonal: body.telefonoPersonal,
    });

    newSchemaPersonal
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok: true,
                message:'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok: false,
                message:'Hubo problemas',
                err
            });
        }
    )
});

app.get('/obtener/personal', async (req, res) =>{
    const respuesta = await modelPersonal.find();
        res.json({
            ok: true,
            respuesta
        });
});

app.delete('/eliminar/personal/:id', async (req, res) => {
    let id = req.params.id
    
    const respuesta = await modelPersonal.findByIdAndDelete(id);
        res.json({
            ok: true,
            respuesta
        });
});

app.put('/actualizar/personal/:id', async (req, res) =>{
    let id = req.params.id;
    let campos = req.body;
    const respuesta = await modelPersonal.findByIdAndUpdate(id,campos,{new:true});
        res.json({
            ok: true,
            respuesta
        })    
});

module.exports = app;