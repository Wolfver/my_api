const express = require('express');
const { request } = require('../test');
const modelNuevoProducto = require('../../Models/refaccionaria/modelNuevoProducto');

let app = express();

app.post('/producto/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProducto = new modelNuevoProducto({
    nombreProducto: body.nombreProducto,
    marcaProducto: body.marcaProducto,
    presentacionProducto: body.presentacionProducto,
    contenidoProducto: body.contenidoProducto,
    costoProducto: body.costoProducto,
    proveedorProducto: body.proveedorProducto,
    cantidadIngresa: body.cantidadIngresa,
    statusProducto: body.statusProducto,
    descripcionProducto: body.descripcionProducto,
    });

    newSchemaProducto
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok: true,
                message:'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok: false,
                message:'Hubo problemas',
                err
            });
        }
    )
});

app.get('/obtener/producto', async (req, res) =>{
    const respuesta = await modelNuevoProducto.find();
        res.json({
            ok: true,
            respuesta
        });
});

app.delete('/eliminar/producto/:id', async (req, res) => {
    let id = req.params.id
    
    const respuesta = await modelNuevoProducto.findByIdAndDelete(id);
        res.json({
            ok: true,
            respuesta
        });
});

app.put('/actualizar/producto/:id', async (req, res) =>{
    let id = req.params.id;
    let campos = req.body;
    const respuesta = await modelNuevoProducto.findByIdAndUpdate(id,campos,{new:true});
        res.json({
            ok: true,
            respuesta
        })    
});

module.exports = app;