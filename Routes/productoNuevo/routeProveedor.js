const express = require('express');
const modelProveedor = require('../../Models/refaccionaria//modelProveedor');

let app = express();

app.post('/proveedor/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaProveedor = new modelProveedor({
        IDProveedor: body.IDProveedor,
        nombreProveedor: body.nombreProveedor,
        marcaProveedor: body.marcaProveedor,
        direccionProveedor: body.direccionProveedor,
        telefonoProveedor: body.telefonoProveedor,

    });

    newSchemaProveedor
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok: true,
                message:'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok: false,
                message:'Hubo problemas',
                err
            });
        }
    )
});

app.get('/obtener/proveedor', async (req, res) =>{
    const respuesta = await modelProveedor.find();
        res.json({
            ok: true,
            respuesta
        });
});

app.delete('/eliminar/proveedor/:id', async (req, res) => {
    let id = req.params.id
    
    const respuesta = await modelProveedor.findByIdAndDelete(id);
        res.json({
            ok: true,
            respuesta
        });
});

app.put('/actualizar/proveedor/:id', async (req, res) =>{
    let id = req.params.id;
    let campos = req.body;
    const respuesta = await modelProveedor.findByIdAndUpdate(id,campos,{new:true});
        res.json({
            ok: true,
            respuesta
        })    
});

module.exports = app;