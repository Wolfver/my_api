const express = require('express');
//const modelPersonal = require('../../Models/refaccionaria/modelPersonal');
const { request } = require('../test');
const modelSucursal = require('../../Models/refaccionaria/modelSucursal');

let app = express();

app.post('/sucursal/nuevo', (req, res) => {
    let body = req.body;
    console.log(body);

    let newSchemaSucursal = new modelSucursal({
        IDSucursal: body.IDSucursal,
        nombreSucursal: body.nombreSucursal,
        estadoSucursal: body.estadoSucursal,
        direccionSucursal: body.direccionSucursal,
        telefonoSucursal: body.telefonoSucursal,
    });

    newSchemaSucursal
    .save()
    .then(
        (data) => {
            return res.status(200)
            .json({
                ok: true,
                message:'Datos guardados',
                data
            });
        }
    )
    .catch(
        (err) => {
            return res.status(500)
            .json({
                ok: false,
                message:'Hubo problemas',
                err
            });
        }
    )
});

app.get('/obtener/sucursal', async (req, res) =>{
    const respuesta = await modelSucursal.find();
        res.json({
            ok: true,
            respuesta
        });
});

app.delete('/eliminar/sucursal/:id', async (req, res) => {
    let id = req.params.id
    
    const respuesta = await modelSucursal.findByIdAndDelete(id);
        res.json({
            ok: true,
            respuesta
        });
});

app.put('/actualizar/sucursal/:id', async (req, res) =>{
    let id = req.params.id;
    let campos = req.body;
    const respuesta = await modelSucursal.findByIdAndUpdate(id,campos,{new:true});
        res.json({
            ok: true,
            respuesta
        })    
});

module.exports = app;